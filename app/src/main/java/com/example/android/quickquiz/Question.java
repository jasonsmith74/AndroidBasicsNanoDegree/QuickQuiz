package com.example.android.quickquiz;

/**
 * Created by jason on 1/17/2018.
 */

    public class Question {
        private String text;
        private String type;
        private String [] choices;
        private int [] correctAnswers;

        public Question (String textIn, String typeIn, String [] choicesIn, int [] answersIn) {
            text = textIn;
            type = typeIn;
            choices = choicesIn;
            correctAnswers = answersIn;
        }

        public void setText (String textIn){
                text = textIn;
        }

        public String getText (){
                return text;
        }

        public void setChoices (String [] choicesIn){
            choices = choicesIn;
        }

        public String [] getChoices (){
            return choices;
        }

        public void setCorrectAnswers (int [] answersIn){
            correctAnswers = answersIn;
        }

        public int [] getCorrectAnswers(){
            return correctAnswers;
        }
    }

 /*   public class textQuestion extends Question{
        public textQuestion (){};
    }*/

