package com.example.android.quickquiz;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import static com.example.android.quickquiz.MainActivity.getQuizResources;
import static com.example.android.quickquiz.MainActivity.ResArrayType;

/**
 * A simple {@link Fragment} subclass.
  * Use the {@link TextFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TextFragment extends Fragment {
    private static final String QUESTION_NUMBER = "question_number";
    private int questionNumber;
    private ViewGroup rootView;

    public TextFragment() {
        // Required empty public constructor
    }

    /**
     * Returns a new instance of this fragment for the given
     * @param questionNumber the position of this fragment in the adapter
     *
     */
    public static TextFragment newInstance(int questionNumber) {
        TextFragment fragment = new TextFragment();
        Bundle args = new Bundle();
        args.putInt(QUESTION_NUMBER, questionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            questionNumber = getArguments().getInt(QUESTION_NUMBER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_text, container,
                false);
        TextView textView = rootView.findViewById(R.id.text_question_text);
        //TODO check for automatic handling of missing resources. see checkResourceSizes method in MainActivity
        textView.setText(getQuizResources(ResArrayType.QUESTIONS)[questionNumber]);
        return rootView;
    }

    public void setAnswer(String answer){
        TextView txt = (TextView)rootView.findViewById(R.id.text_answer);
        txt.setText(answer);
    }

    public String getUserAnswer(){
        EditText txt = (EditText) rootView.findViewById(R.id.text_answer);
        return String.valueOf(txt.getText());
    }
}
