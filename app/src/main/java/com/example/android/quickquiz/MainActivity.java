package com.example.android.quickquiz;

import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends FragmentActivity implements ScoreFragment.onScoreListener, MCMultipleAnswerFragment.OnFragmentInteractionListener{
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private static final String [] NO_STRINGS = {};
    public static Resources QUIZ_RESOURCES;//hold reference created in onCreate below
    public static String PACKAGE_NAME;//assign value in onCreate below
    private SectionsPagerAdapter mSectionsPagerAdapter;
    public static int NUM_QUESTIONS;//assign value in onCreate below
    public static QuestionType [] QUESTION_TYPES;
    public static int numberCorrect = 0;
    public enum ResArrayType {QUESTIONS, CHOICES, ANSWERS, SINGLE_ANSWER, TEXT_ANSWER}
    public enum QuestionType {MCMultipleAnswerFragment, MCSingleAnswerFragment, TextFragment}


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get reference to package resources from app\res\*,xml
        PACKAGE_NAME = getApplicationContext().getPackageName();
        QUIZ_RESOURCES = getBaseContext().getResources();
        NUM_QUESTIONS = getQuizResources(ResArrayType.QUESTIONS).length;
        QUESTION_TYPES = getQuestTypes();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(NUM_QUESTIONS+1);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }
    /*
    TODO check for automatic handling of mismatches in resources > Possibly get rid of these methods.
    TODO if not getting rid of these methods, throw MissingResourceException if needed
    TODO use formatted resource strings in place of literals
    TODO override again to handle text_fragment types
    */

    public QuestionType [] getQuestTypes (){
        QuestionType [] types = new QuestionType[NUM_QUESTIONS];
        for (int index = 0; index < NUM_QUESTIONS; index++){
            if (getQuizResources(ResArrayType.ANSWERS, index).length != 0){
                types[index] = QuestionType.MCMultipleAnswerFragment;
            }else if (getQuizResources(ResArrayType.SINGLE_ANSWER, index).length != 0){
                types[index] = QuestionType.MCSingleAnswerFragment;
            }else if (getQuizResources(ResArrayType.TEXT_ANSWER, index).length != 0){
                types[index] = QuestionType.TextFragment;
            }
        }
        return types;
    }

    /** This method checks that resources were defined correctly in files:
     * fragment_mc_multiple_answer and questions_answers.xml
     * This is for debug purposes only such as deploying new resource files.
     * @param answersArray answers array from resource file
     * @param choicesArray choices array from resource file
     * @param viewGroup group of checkboxes view in layout file
     * @param questionNumber the position/index of the questions array
     */
    public static void checkResourceSizes (String [] answersArray,
                                           String [] choicesArray,
                                           ViewGroup viewGroup,
                                           int questionNumber){
        if ((choicesArray.length != answersArray.length) || (viewGroup.getChildCount()
                != answersArray.length)) {
            Log.d("CheckboxesGroupCreation",
                    "The size of these must match: " +
                            "id.choices in app\\res\\layout\\fragment_mc_multiple_answer.xml\n" +
                            "question_" + String.valueOf(questionNumber) + "_choices" + ", and" +
                            "question_" + String.valueOf(questionNumber) + "_answers in " +
                            "app\\res\\values\\question_answers.xml");
        }
    }

    /** This method checks that resources were defined correctly in files:
     * fragment_mc_single_answer and questions_answers.xml
     * This is for debug purposes only such as deploying new resource files.
     * This method overrides previous method with same name.
     * @param choicesArray choices array from resource file
     * @param viewGroup group of checkboxes view in layout file
     * @param questionNumber the position/index of the questions array
     */
    public static void checkResourceSizes(String[] choicesArray,
                                          ViewGroup viewGroup,
                                          int questionNumber){

        if (choicesArray.length != viewGroup.getChildCount()) {
            Log.d("RadioGroupCreation",
                    "The size of these must match: " +
                            "id.choices in app\\res\\layout\\fragment_mc_single_answer.xml\n" +
                            "question_" + String.valueOf(questionNumber) + "_choices" + ", and" +
                            "question_" + String.valueOf(questionNumber) + "_single_answer in " +
                            "app\\res\\values\\question_answers.xml");
        }
    }

    /**
     * This method returns an array of strings stored in resources based on type
     * @param type an item from enum ResArrayType
     * @param questionNumber the position/index of the questions array, unused if type = QUESTIONS
     * @return array of resource strings. Will have size of 1 if type = SINGLE_ANSWER||TEXT_ANSWER
     */
    public static String [] getQuizResources(ResArrayType type, int questionNumber){
        int resId;
        switch (type){
            case CHOICES:
            case ANSWERS:
                resId = QUIZ_RESOURCES.getIdentifier("question_" +
                    String.valueOf(questionNumber) + "_" + String.valueOf(type).toLowerCase(),
                        "array", PACKAGE_NAME);
                if (resId == 0) return NO_STRINGS; else return QUIZ_RESOURCES.getStringArray(resId);
            case QUESTIONS:
                resId = QUIZ_RESOURCES.getIdentifier(String.valueOf(type).toLowerCase(),
                        "array", PACKAGE_NAME);
                if (resId == 0) return NO_STRINGS; else return QUIZ_RESOURCES.getStringArray(resId);
            case SINGLE_ANSWER:
            case TEXT_ANSWER:
                resId = QUIZ_RESOURCES.getIdentifier("question_" +
                                String.valueOf(questionNumber) + "_" +
                                String.valueOf(type).toLowerCase(),
                        "string", PACKAGE_NAME);
                if (resId == 0) return NO_STRINGS; else return new String[]{QUIZ_RESOURCES.getString(resId)};
            default://should never happen
        }
        return null; //should never happen
    }

    /**
     * This method is overloaded to be used if type = QUESTIONS
     * @param type an item from enum ResArrayType
     * @return array of resource strings.
     */
    public static String [] getQuizResources(ResArrayType type) {
        return getQuizResources(type, -1);
    }

    public void onScorePressed(){
        String [] questAnswers;
        MainActivity.numberCorrect = 0;
        boolean ansIsCorrect;
        int answersResId;

        //check each questions answer based on fragClass type
        for (int quesIndex = 0; quesIndex < NUM_QUESTIONS; quesIndex++) {
            //get reference to fragment in order it was created as a Fragment base type
            Fragment fragment = mSectionsPagerAdapter.fragments[quesIndex];
            String fragClass = fragment.getClass().getName();
            //remove package name from class name
            fragClass = fragClass.replace(PACKAGE_NAME + ".","");
            switch (fragClass){
                case "MCMultipleAnswerFragment":
                    ansIsCorrect = true;
                    MCMultipleAnswerFragment mcmaAnswerFrag = (MCMultipleAnswerFragment) fragment;
                    ViewGroup mcmaRootView = (ViewGroup) mcmaAnswerFrag.getView();
                    ViewGroup checkBoxes = (ViewGroup) mcmaRootView.findViewById(R.id.choices);
                    questAnswers = getQuizResources(ResArrayType.ANSWERS, quesIndex);
                    for (int ansIndex = 0; ansIndex < questAnswers.length; ansIndex++){
                        ansIsCorrect = ansIsCorrect && ((CheckBox) checkBoxes.getChildAt(ansIndex))
                                .isChecked() == Boolean.parseBoolean(questAnswers[ansIndex]);
                    }
                    if(ansIsCorrect){numberCorrect++;}
                    break;
                case "MCSingleAnswerFragment":
                    ansIsCorrect = true;
                    MCSingleAnswerFragment mc1AnswerFragment = (MCSingleAnswerFragment) fragment;
                    ViewGroup mcsaRootView = (ViewGroup) mc1AnswerFragment.getView();
                    RadioGroup rgButtons = (RadioGroup) mcsaRootView.findViewById(R.id.choices);
                    int selectedId = rgButtons.getCheckedRadioButtonId(); // get the selected button
                    RadioButton radioChecked = rgButtons.findViewById(selectedId);
                    questAnswers = getQuizResources(ResArrayType.SINGLE_ANSWER, quesIndex);
                    //use 0 as array index since this is a single answer question
                    ansIsCorrect =
                            (radioChecked.getText().toString().equalsIgnoreCase(questAnswers[0]));
                    if(ansIsCorrect){numberCorrect++;}
                    break;
                case "TextFragment":
                    TextFragment textfragment = (TextFragment) fragment;
                    //use 0 as array index since this is a single answer question
                    String correctAnswer = getQuizResources(ResArrayType.TEXT_ANSWER, quesIndex)[0];
                    String userAnswer = textfragment.getUserAnswer().trim();
                    ansIsCorrect = correctAnswer.equalsIgnoreCase(userAnswer);
                    if(ansIsCorrect){numberCorrect++;}
                    break;
                default:
            }
        }
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, "Score = " + String.valueOf(numberCorrect), duration);
        toast.show();
    }

    public void onFragmentInteraction(Uri uri){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, "MCMA URI = " + String.valueOf(uri), duration);
        toast.show();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        //This will contain the Fragment references, including questions and 1 final SCORE page.
        public Fragment[] fragments = new Fragment[NUM_QUESTIONS+1];

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // a reference to each new fragment is stored in fragments[]
            if (position < NUM_QUESTIONS){
            QuestionType type = QUESTION_TYPES[position];
                switch (type) {
                    case MCMultipleAnswerFragment:
                        fragments[position] = MCMultipleAnswerFragment.newInstance(position);
                        break;
                    case MCSingleAnswerFragment:
                        fragments[position] = MCSingleAnswerFragment.newInstance(position);
                        break;
                    case TextFragment:
                        fragments[position] = TextFragment.newInstance(position);
                        break;
                    default: fragments[position] = null; //should not occur
                }
            } else  fragments[position] = ScoreFragment.newInstance();
            return fragments[position];
        }

        @Override
        public int getCount() {
            // Show total pages including questions and 1 final SCORE page.
            return NUM_QUESTIONS+1;
        }
    }
}

