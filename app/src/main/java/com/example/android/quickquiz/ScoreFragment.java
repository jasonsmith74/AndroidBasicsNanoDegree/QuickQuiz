package com.example.android.quickquiz;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A fragment with a Score button.
 * Activities that contain this fragment must implement the
 * {@link ScoreFragment.onScoreListener} interface
 * to handle interaction events.
 * Use the {@link ScoreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScoreFragment extends Fragment {
/*    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;*/

    onScoreListener mListener;

    public ScoreFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * @return A new instance of fragment ScoreFragment.
     */
    public static ScoreFragment newInstance() {
        return new ScoreFragment();
    }

/*    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
/*        View view = inflater.inflate(R.layout.fragment_score, container, false);
        return view;*/
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_score, null);
        init(root);
        return root;
    }

    void init(ViewGroup root){
        Button but=(Button)root.findViewById(R.id.score_button);
        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mListener.onScorePressed();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof onScoreListener) {
            mListener = (onScoreListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement onScoreListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface onScoreListener {
        // defined in MainActivity
        void onScorePressed();
    }
}
