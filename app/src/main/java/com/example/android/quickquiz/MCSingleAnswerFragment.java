package com.example.android.quickquiz;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.example.android.quickquiz.MainActivity.ResArrayType;
import static com.example.android.quickquiz.MainActivity.getQuizResources;
import static com.example.android.quickquiz.MainActivity.checkResourceSizes;

/**
 * A multiple choice single answer fragment with radio buttons
 */
public class MCSingleAnswerFragment extends Fragment {
    private static final String QUESTION_NUMBER = "question_number";
/*    private static final String QUESTIONS ="questions";
    private static final String CHOICES ="choices";
    private static final String ANSWERS ="answers";*/
    private int questionNumber;
    private ViewGroup rootView;

    public MCSingleAnswerFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given
     * @param questionNumber the position of this fragment in the adapter
     *
     */
    public static MCSingleAnswerFragment newInstance(int questionNumber) {
        MCSingleAnswerFragment fragment = new MCSingleAnswerFragment();
        Bundle args = new Bundle();
        args.putInt(QUESTION_NUMBER, questionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            questionNumber = getArguments().getInt(QUESTION_NUMBER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_mc_single_answer,
                container,false);
        String [] questions = getQuizResources(ResArrayType.QUESTIONS);
        String [] questionChoices = getQuizResources(ResArrayType.CHOICES, questionNumber);
        RadioGroup radioButtons = (RadioGroup) rootView.findViewById(R.id.choices);
        //TODO Make RadioGroup button code dynamic based on Questions in questions.xml resource
        checkResourceSizes (questionChoices, radioButtons, questionNumber);
        TextView textView = (TextView) rootView.findViewById(R.id.question_text);
        textView.setText(questions[questionNumber]);
        //set text for each radio button in the RadioGroup
        for (int choiceIndex = 0; choiceIndex < radioButtons.getChildCount(); choiceIndex++) {
            ((RadioButton) radioButtons.getChildAt(choiceIndex)).
                    setText(questionChoices[choiceIndex]);
        }
        return rootView;
    }

    public boolean isChecked (int choiceIndex){
        RadioGroup radioButtons = (RadioGroup) rootView.findViewById(R.id.choices);
        RadioButton oneButton = (RadioButton) radioButtons.getChildAt(choiceIndex);
        return oneButton.isChecked();
    }
}
