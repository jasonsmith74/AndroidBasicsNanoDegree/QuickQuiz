package com.example.android.quickquiz;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.example.android.quickquiz.MainActivity.ResArrayType;
import static com.example.android.quickquiz.MainActivity.getQuizResources;
import static com.example.android.quickquiz.MainActivity.checkResourceSizes;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MCMultipleAnswerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MCMultipleAnswerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MCMultipleAnswerFragment extends Fragment {
    private static final String QUESTION_NUMBER = "question_number";
    private int questionNumber;
    private ViewGroup rootView;

    private OnFragmentInteractionListener mListener;

    public MCMultipleAnswerFragment() {
        // Required empty public constructor
    }

        /**
         * Returns a new instance of this fragment for the given question
         * number.
         */
        public static MCMultipleAnswerFragment newInstance(int questionNumber) {
            MCMultipleAnswerFragment fragment = new MCMultipleAnswerFragment();
            Bundle args = new Bundle();
            args.putInt(QUESTION_NUMBER, questionNumber);
            fragment.setArguments(args);
            return fragment;
        }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            questionNumber = getArguments().getInt(QUESTION_NUMBER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_mc_multiple_answer,
                container, false);
        initLayout();
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void initLayout(){
        String [] questions = getQuizResources(ResArrayType.QUESTIONS);
        String [] questionChoices = getQuizResources(ResArrayType.CHOICES, questionNumber);
        String [] questionAnswers = getQuizResources(ResArrayType.ANSWERS, questionNumber);
        ViewGroup checkBoxes = (ViewGroup) rootView.findViewById(R.id.choices);
        //TODO check for automatic handling of missing resources. see checkResourceSizes method in MainActivity
        checkResourceSizes (questionAnswers, questionChoices, checkBoxes, questionNumber);
        TextView textView = (TextView) rootView.findViewById(R.id.question_text);
        textView.setText(questions[questionNumber]);
        //set text for each checkbox in ViewGroup
        for (int choiceIndex = 0; choiceIndex < checkBoxes.getChildCount(); choiceIndex++) {
            ((CheckBox) checkBoxes.getChildAt(choiceIndex)).
                    setText(questionChoices[choiceIndex]);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
